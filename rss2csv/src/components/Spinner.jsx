import React from "react";
import {PulseLoader} from 'react-spinners';
import skin from "../skin";

const Spinner = (props) => {
    return (
        <div style={{marginTop: "28px"}}>
            <PulseLoader color={skin.accent} />
        </div>
    );
};

export default Spinner;