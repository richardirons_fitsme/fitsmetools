import React from "react";
import skin from "../skin";

const bodyStyle = {
    backgroundColor: skin.background,
    color: skin.textColor,
    fontFamily: skin.normalFontSize,
    fontSize: skin.normalFontSize,
    lineHeight: skin.normalLineHeight
};

const tableStyle = {
    color : skin.textColor,
    borderCollapse: "collapse",
    minWidth: "300px"
};

const tableHeaderStyle = {
    backgroundColor: skin.accent,
    color: "white",
    fontWeight: "bold",
    fontFamily: skin.fontFamily,
    fontSize: skin.compactFontSize,
    lineHeight: skin.compactLineHeight,
    textAlign: "left",
    padding: "6px",
    paddingTop: "5px",
    paddingBottom: "5px",
    border: "1px solid " + skin.borderColor
};

const tableCellStyle = {
    color: skin.textColor,
    fontFamily: skin.fontFamily,
    fontSize: skin.compactFontSize,
    lineHeight: skin.compactLineHeight,
    textAlign: "left",
    paddingLeft: "6px",
    paddingRight: "6px",
    border: "1px solid " + skin.borderColor
}

const bodyTextStyle = {
    color: skin.textColor,
    fontFamily: skin.fontFamily,
    fontSize: skin.normalFontSize,
    lineHeight: skin.normalLineHeight
}

const loadingTextStyle = {
    color: skin.textColor,
    fontFamily: skin.loadingFontFamily,
    fontSize: skin.loadingFontSize,  
}

export {
    bodyStyle,
    tableStyle,
    tableHeaderStyle,
    tableCellStyle,
    loadingTextStyle,
    bodyTextStyle
};