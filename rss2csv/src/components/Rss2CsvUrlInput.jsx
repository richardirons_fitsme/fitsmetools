import React from "react";

export default class Rss2CsvUrlInput extends React.Component {
    state = {
        inputText: ""
    };

    constructor(props) {
        super(props);
        this.onButtonClick = props.onButtonClick;
    }

    updateInputText = event => this.setState({inputText : event.target.value});

    render() {
        return (
            <div style={{paddingBottom:  "10px"}}>
                <input 
                    type="text" 
                    value={this.state.inputText} 
                    onChange={e => this.updateInputText(e)}
                    style={{minWidth: "400px", marginRight:"16px"}} 
                    placeholder="Enter the URL of an RSS feed"/>

                <button 
                    onClick={() => this.onButtonClick(this.state.inputText)}>
                        Fetch
                    </button>
            </div>
        );
    }
};