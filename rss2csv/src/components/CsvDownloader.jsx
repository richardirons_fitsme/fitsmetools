import React from "react";

const CsvDownloader = props => {
    return (
        <button
            onClick={props.buttonClick}>
            Download as CSV
        </button>
    );
};

export default CsvDownloader;