import React from "react";
import {tableHeaderStyle} from "./Rss2CsvStyles";

const RssTableHeader = props => {
    return (
        <thead>
            <tr>
                {props.columnHeaders.map((x, i) => 
                    <th key={i.toString()} style={tableHeaderStyle}>
                        {x}
                    </th>
                )}
            </tr>
        </thead>
    );
};

export default RssTableHeader;