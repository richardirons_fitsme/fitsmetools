import React from "react";
import Linkify from 'react-linkify';

import {tableCellStyle} from "Rss2CsvStyles"

const RssTableBody = props => {
    var headers = props.columnHeaders;
    var data = props.data;

    return (
        <tbody>
            {data.map((di, idx) => 
                <tr key={idx}>
                    {headers.map(h => 
                        <td key={h} style={tableCellStyle}>
                            <Linkify>{di[h]}</Linkify>
                        </td>)}
                </tr>)}
        </tbody>  
    );
};

export default RssTableBody;