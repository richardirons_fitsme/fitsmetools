import React from "react";
import RssTable from "RssTable";
import Rss2CsvUrlInput from "Rss2CsvUrlInput";
import Spinner from "Spinner";
import {bodyStyle, bodyTextStyle} from "Rss2CsvStyles";
import downloadCsv from "../downloadCsv";
import getJsonFromRss from "../getJsonFromRss";

export default class Rss2CsvApp extends React.Component {
    state = {
        displayContent: {
            totalCount: 0,
            displayCount: 0,
            columnHeaders: [],
            data: []
        },
        fullContent: {},
        spinnerVisible: false
    };

    setRssUrl = url => {
        this.setState({ spinnerVisible: true });
        getJsonFromRss(url)
            .then(jsonobj => {
                this.setState(jsonobj);
                this.setState({ spinnerVisible : false });
            });
    };

    triggerCsvDownload = () => {
        downloadCsv(this.state.fullContent.columnHeaders, this.state.fullContent.data);
    }

    componentWillMount = () => {
        for(let i in bodyStyle) {
            document.body.style[i] = bodyStyle[i];
        }
    };

    componentWillUnmount = () => {
        for(let i in bodyStyle) {
            document.body.style[i] = null;
        }
    };

    render() {
        return (
            <div style={bodyTextStyle}>
                <Rss2CsvUrlInput onButtonClick={this.setRssUrl} />
                { this.state.displayContent.displayCount != 0 && <hr/> }
                { this.state.spinnerVisible 
                    ? <Spinner/> 
                    : <RssTable 
                        displayData={this.state.displayContent} 
                        downloadButtonClicked={this.triggerCsvDownload} /> 
                }
            </div>
        );
    }
}
