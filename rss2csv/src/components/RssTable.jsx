import React from "react";
import RssTableHeader from "RssTableHeader";
import RssTableBody from "RssTableBody";
import CsvDownloader from "CsvDownloader";
import {tableStyle, bodyTextStyle} from "Rss2CsvStyles"

const RssTable = (props) => {
    return (
        <div>
            <div style={Object.assign({}, bodyTextStyle, {marginBottom: "14px"})}>
                { 
                    props.displayData.displayCount != 0 &&
                        <span>
                            <span style={{marginRight: "16px"}}>
                                Showing {props.displayData.displayCount} of {props.displayData.totalCount} row(s):
                            </span>
                            <CsvDownloader buttonClick={props.downloadButtonClicked} />
                        </span>
                }
            </div>
            <table style={tableStyle}>
                <RssTableHeader columnHeaders={props.displayData.columnHeaders}/>
                <RssTableBody columnHeaders={props.displayData.columnHeaders} data={props.displayData.data}/>
            </table>
        </div>
    );
};

export default RssTable;