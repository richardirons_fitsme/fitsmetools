import xml2js from "xml2js";
import { query } from "jsonpath";

// gets everything after the last colon, or the whole string if there isn't a colon
const getLocalName = name => /[^:]*$/.exec(name)[0];

// transforms an RSS feed in json format into an object with headers and data rows
const transformJson = (jsonData) => {
    const queryResult = query(jsonData, "$..item");
    const items = [].concat.apply([], queryResult);

    let headers = {};
    let dataItems = [];

    for(let item of items) {
        let dataItem = {};
        const keys = Object.keys(item);

        for(let key of keys) {
            let value = item[key];
            if(Array.isArray(value)){
                value = value[0];
            }

            const localName = getLocalName(key);
            if(typeof(value) !== "object" && !dataItem[localName]) {
                headers[localName] = true;
                dataItem[localName] = value;
            }
        }

        dataItems.push(dataItem);
    }
    
    const result = {
        columnHeaders: Object.keys(headers),
        data: dataItems
    };

    const displayCount = 100;
    const displayItems = dataItems.slice(0, displayCount);
    const displayResult = {
        columnHeaders: Object.keys(headers),
        data: displayItems,
        totalCount: dataItems.length,
        displayCount: displayItems.length
    }

    return {
        fullContent: result,
        displayContent: displayResult
    }
}

const getJsonFromRss = (rssurl) => {
    return fetch(`https://cors-anywhere.herokuapp.com/${rssurl}`)
        .then(response => response.text())
        .then(xml => 
            new Promise((resolve, reject) => {
                xml2js.parseString(xml, (err, result) => {
                    resolve(transformJson(result));
                })
            })
        );
};

export default getJsonFromRss;
