const enquote = text => `"${text.replace(/\"/g, "\"\"")}"`;

// turns a list of headers and a bunch of "rows" (dictionaries
// keyed by those headers) into a standard format CSV file
// (in memory)
const getCsvData = (headers, rows) => {
    let lines = [];
    const headerLine = headers.map(h => enquote(h)).join(",");
    lines.push(headerLine);

    for(let row of rows) {
        let lineItems = [];
        for(let header of headers) {
            let value = row[header];
            if(typeof(value) === "undefined") {
                value = "";
            }
            lineItems.push(enquote(value));
        }

        const line = lineItems.join(",");
        lines.push(line);
    }

    return lines.join("\r\n");
};

export default getCsvData;