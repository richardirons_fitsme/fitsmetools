const skin = {
    background: "#white",
    textColor: "black",
    accent: "#006182",
    alternateBackground: "#DFDFDF",
    borderColor: "#dfdfdf",
    fontFamily: "Trebuchet MS, Arial, sans-serif",
    normalFontSize: "10pt",
    normalLineHeight: "14pt",
    compactFontSize: "8pt",
    compactLineHeight: "12pt",
    loadingFontFamily: "Trebuchet MS, sans-serif",
    loadingFontSize: "12pt"
};

export default skin;