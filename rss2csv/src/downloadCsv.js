import getCsvData from "getCsvData";

const linkId = "¦downloadCsvFakeLink¦";

const downloadCsv = (headers, rows) => {
    const csvData = getCsvData(headers, rows);
    const blob = new Blob(["\ufeff", csvData]);
    let downloadLink = document.getElementById(linkId);
    if(downloadLink == null) {
        downloadLink = document.createElement("a");
        downloadLink.setAttribute('download', 'DownloadedFile.csv');
        downloadLink.setAttribute('id', linkId);
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
    }

    downloadLink.href = URL.createObjectURL(blob);
    downloadLink.click();
};

export default downloadCsv;