import HTMLWebpackPlugin from "html-webpack-plugin";

const HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
    template: __dirname + '/src/index.html',
    filename: __dirname + '/index.html',
    inject: 'body'
});

export const entry = __dirname + '/src/index.js';

export const module = {
    rules: [{
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
    }]
};

export const output = {
    filename: 'transformed.js',
    path: __dirname + '/build'
};

export const plugins = [HTMLWebpackPluginConfig];
export const mode = 'development';
export const stats = { colors: true };

export const resolve = {
    extensions: [".webpack.js", ".web.js", ".js", ".json", ".jsx"],
    modules: ["web_modules", "node_modules", "src", "src/components"]
};

export const node = {
    fs: "empty"
};